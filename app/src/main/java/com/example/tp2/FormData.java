package com.example.tp2;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormData implements Parcelable {
    private String type;
    private String name;
    private String value;
    private String desc;
    private Integer index;
    private Integer validator;

    FormData(){}

    FormData(String type, String name, String value, String desc, Integer index, Integer validator){
        this.setType(type);
        this.setName(name);
        this.setValue(value);
        this.setDesc(desc);
        this.setIndex(index);
        this.setValidator(validator);
    }

    protected FormData(Parcel in) {
        type = in.readString();
        name = in.readString();
        value = in.readString();
        desc = in.readString();
        index = in.readInt();
        validator = in.readInt();
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static Pattern DATE_PATTERN =
            Pattern.compile("^\\d{4}/\\d{2}/\\d{2}$");

    private static Pattern TELEPHONE =
            Pattern.compile("[0-9]*-?");

    private static Pattern ONLY_LETTERS_AND_SPACES =
            Pattern.compile("[a-z\\s]*", Pattern.CASE_INSENSITIVE);


    private abstract class Validator{
        abstract boolean validate(String arg);
    }

    private FormData.Validator[] validators = new FormData.Validator[]{
            new Validator() { public boolean validate(String arg) { return true; } },
            new Validator() { public boolean validate(String arg) { return validateNombre(arg); } },
            new Validator() { public boolean validate(String arg) { return validateApellido(arg); } },
            new Validator() { public boolean validate(String arg) { return validateTelefono(arg); } },
            new Validator() { public boolean validate(String arg) { return validateEmail(arg); } },
            new Validator() { public boolean validate(String arg) { return validateFechaNac(arg); } },
            new Validator() { public boolean validate(String arg) { return validateNotEmpty(arg); } },
    };

    public boolean validators(int index, String arg){
        return validators[index].validate(arg);
    }

    private boolean validateNotEmpty(String arg){
        return (arg.length() > 0);
    }

    private boolean validateNombre(String arg){
        return (ONLY_LETTERS_AND_SPACES.matcher(arg).matches() &&  validateNotEmpty(arg));
    }

    private boolean validateApellido(String arg){
        return (ONLY_LETTERS_AND_SPACES.matcher(arg).matches() && validateNotEmpty(arg));
    }

    private boolean validateTelefono(String arg){
        return (TELEPHONE.matcher(arg).matches() && validateNotEmpty(arg));
    }

    private boolean validateEmail(String arg){
        return VALID_EMAIL_ADDRESS_REGEX.matcher(arg).matches();
    }

    private boolean validateFechaNac(String arg){
        return DATE_PATTERN.matcher(arg).matches();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(name);
        dest.writeString(value);
        dest.writeString(desc);
        dest.writeInt(index);
        dest.writeInt(validator);
    }

    public static final Creator<FormData> CREATOR = new Creator<FormData>() {
        @Override
        public FormData createFromParcel(Parcel in) {
            return new FormData(in);
        }

        @Override
        public FormData[] newArray(int size) {
            return new FormData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getIndex() { return index; }

    public Integer getValidator() { return validator; }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setIndex(Integer index) { this.index = index; }

    public void setValidator(Integer validator) { this.validator = validator; }
}
