package com.example.tp2;

import java.io.Serializable;

public class Persona implements Serializable {

    String nombre;
    String apellido;
    String telefono;
    String telefonoLugar;
    String email;
    String emailLugar;
    String direccion;
    String fechaNac;
    String estudios;
    String intereses;
    String enviarInfo;

    public Persona(String nombre,String apellido,String telefono,String telefonoLugar,String email,String emailLugar,String direccion,String fechaNac,String estudios,String intereses,String enviarInfo){
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.telefonoLugar = telefonoLugar;
        this.email = email;
        this.emailLugar = emailLugar;
        this.direccion = direccion;
        this.fechaNac = fechaNac;
        this.estudios = estudios;
        this.intereses = intereses;
        this.enviarInfo = enviarInfo;
    }

    public Persona() {

    }

    private abstract class Setter implements Serializable{
        abstract void set(String arg);
    }

    private Setter[] setters = new Setter[]{
            new Setter() { public void set(String arg) { setNombre(arg); } },
            new Setter() { public void set(String arg) { setApellido(arg); } },
            new Setter() { public void set(String arg) { setTelefono(arg); } },
            new Setter() { public void set(String arg) { setTelefonoLugar(arg); } },
            new Setter() { public void set(String arg) { setEmail(arg); } },
            new Setter() { public void set(String arg) { setEmailLugar(arg); } },
            new Setter() { public void set(String arg) { setDireccion(arg); } },
            new Setter() { public void set(String arg) { setFechaNac(arg); } },
            new Setter() { public void set(String arg) { setEstudios(arg); } },
            new Setter() { public void set(String arg) { setIntereses(arg); } },
            new Setter() { public void set(String arg) { setEnviarInfo(arg); } },
    };

    public void set(int index, String arg){
        setters[index].set(arg);
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setTelefonoLugar(String telefonoLugar) {
        this.telefonoLugar = telefonoLugar;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmailLugar(String emailLugar) {
        this.emailLugar = emailLugar;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public void setEstudios(String estudios) {
        this.estudios = estudios;
    }

    public void setIntereses(String intereses) {
        this.intereses = intereses;
    }

    public void setEnviarInfo(String enviarInfo) {
        this.enviarInfo = enviarInfo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getTelefonoLugar() {
        return telefonoLugar;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailLugar() {
        return emailLugar;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public String getEstudios() {
        return estudios;
    }

    public String getIntereses() {
        return intereses;
    }

    public String isEnviarInfo() {
        return enviarInfo;
    }

    public String getFormattedData(){
        return this.getApellido() + " " + this.getNombre() + " - " + this.getEmail();
    }

}
