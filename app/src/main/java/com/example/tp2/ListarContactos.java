package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.arch.core.executor.DefaultTaskExecutor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ListarContactos extends AppCompatActivity {

    String dataFile = "data.obj";
    public ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listar_contactos);

        listView = findViewById(R.id.listView);

        try {
            ObjectInputStream objInput = new ObjectInputStream(openFileInput(dataFile));
            Persona[] persona = (Persona[]) objInput.readObject();
            String[] aux = new String[persona.length];
            objInput.close();
            String Datos = "";
            for (int i = 0; i < persona.length; i++) {
                //Datos = Datos + persona[i].getFormattedData() + "\n";
                aux[i] = persona[i].getFormattedData();
            }

            ArrayAdapter <String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,aux);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(ListarContactos.this, "Hola " + position, Toast.LENGTH_SHORT).show();
                    exec_activity(null, DetalleContacto.class,position);
                }
            });

            //txtLista.setText(Datos);
            //Toast.makeText(getApplicationContext(), "Archivo cargado correctamente", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void exec_activity(View view, Class dest, int posicion){
        //Toast.makeText(getApplicationContext(),et.getText() , et.getText().length()).show();
        Intent i = new Intent(this, dest);
        i.putExtra("posicion", posicion);
        startActivity(i);
    }
/*
    public void showList() {
        String err = null;
        try{
            ObjectInputStream objInput = new ObjectInputStream(openFileInput(this.dataFile));
            Persona personas = (Persona) objInput.readObject();
            objInput.close();

            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            setContentView(linearLayout);

            TextView textView = new TextView(this);
            textView.setText(personas.getFormattedData());
            linearLayout.addView(textView);

            /*
            for( int i = 0; i < personas.size(); i++ )
            {
                TextView textView = new TextView(this);
                textView.setText(personas.get(i).getFormattedData());
                linearLayout.addView(textView);
            }


        }catch (IOException e){
            e.printStackTrace();
            err = "Error al cargar el archivo";
        }catch (ClassNotFoundException e) {
            err = "Error al cargar el archivo";
            e.printStackTrace();
        }catch(Exception e){
            err = "Error al cargar el archivo";
            e.getStackTrace();
            e.printStackTrace();
        }
    }

    public void showList2() {
        try {
            ObjectInputStream objInput = new ObjectInputStream(openFileInput(dataFile));
            Persona[] persona = (Persona[]) objInput.readObject();
            String[] aux = new String[persona.length];
            objInput.close();
            String Datos = "";
            for (int i = 0; i < persona.length; i++) {
                //Datos = Datos + persona[i].getFormattedData() + "\n";
                aux[i] = persona[i].getFormattedData();
            }

            ArrayAdapter <String> adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,aux);
            listView.setAdapter(adapter);

            listView.setOnClickListener(new AdapterView.OnItemClickListener(){

            });

            //txtLista.setText(Datos);
            //Toast.makeText(getApplicationContext(), "Archivo cargado correctamente", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }*/
}