package com.example.tp2;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.BaseKeyListener;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class AgregarContacto extends AppCompatActivity implements  AdapterView.OnItemSelectedListener {

    private ArrayList<FormData> formData;
    private String[] spinnerData = {"Casa", "Trabajo", "Movil"};
    private EditText mail, fecha, nombre, apellido, telefono, direccion;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agregar_contacto);

        mail = findViewById(R.id.edtEmail);
        fecha = findViewById(R.id.edtFecha);
        nombre = findViewById(R.id.edtNombre);
        apellido = findViewById(R.id.edtApellido);
        telefono = findViewById(R.id.edtTelefono);
        direccion = findViewById(R.id.edtDireccion);

        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(AgregarContacto.this,android.R.style.Theme_Holo_Dialog_MinWidth, dateSetListener,year,month,day);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();

            }
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d("MAIN ACTIVITY", "onDateState: date: yyyy/mm/dd: "+year+"/"+month+"/"+day);

                String date = year+"/"+month+"/"+day;
                fecha.setText(date);
            }
        };

        Spinner spinnerEmail = (Spinner) findViewById(R.id.spinnerEmail);
        Spinner spinnerTelefono = (Spinner) findViewById(R.id.spinnerTelefono);
        setSpinner(spinnerEmail);
        setSpinner(spinnerTelefono);

        this.formData = new ArrayList<FormData>();
        this.formData.add( new FormData("EditText", "edtNombre", "", "Nombre", 0, 1) );
        this.formData.add( new FormData("EditText", "edtApellido", "", "Apellido",1, 2) );
        this.formData.add( new FormData("EditText", "edtTelefono", "", "Telefono", 2, 3) );
        this.formData.add( new FormData("Spinner", "spinnerTelefono", "", "", 3, 0) );
        this.formData.add( new FormData("EditText", "edtEmail", "", "Email", 4, 4) );
        this.formData.add( new FormData("Spinner", "spinnerEmail", "", "", 5, 0) );
        this.formData.add( new FormData("EditText", "edtDireccion", "", "Direccion", 6, 6) );
        this.formData.add( new FormData("EditText", "edtFecha", "", "Fecha", 7, 5) );

    }

    private void setSpinner(Spinner spinner){

        spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        //Toast.makeText(getApplicationContext(),spinnerData[position] , Toast.LENGTH_LONG).show();
    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void onClickBtnContinuar(View view){
        ArrayList<FormData> data = new ArrayList<FormData>(formData);
        if( this.validateFields() ){
            for(int i = 0; i < formData.size(); i++){
                data.get(i).setValue( this.getInputText( data.get(i) ) );
            }
            this.exec_activity(null, AgregarContacto2.class, data);
        }
        //if(validarControles()) {
            //this.exec_activity(null, AgregarContacto2.class, data);
        //}
    }

    private String getInputText(FormData node){
        String resp = "";
        int resourceId = getResources().getIdentifier(node.getName(),"id", this.getPackageName());
        switch (node.getType()){
            case "EditText":
                EditText et = (EditText) findViewById(resourceId);
                resp = et.getText().toString();
                break;
            case "Spinner":
                Spinner spinner = (Spinner) findViewById(resourceId);
                resp = spinner.getSelectedItem().toString();
                break;
        }
        return resp;
    }

    private boolean validateFields(){
        boolean resp = true;
        String value = null;
        String err = null;
        FormData field = null;
        for(int i = 0; i < formData.size() && resp; i++){
            field = formData.get(i);
            value = this.getInputText(field);
            if(!field.validators(field.getValidator(), value)){
                resp = false;
                err = "El campo " + field.getDesc() + "='" + value + "' no es valido";
                Toast.makeText(getApplicationContext(), err, err.length()).show();
            }
        }
        return resp;
    }

    private void exec_activity(View view, Class dest, ArrayList<FormData> data){
        //Toast.makeText(getApplicationContext(),et.getText() , et.getText().length()).show();
        Intent i = new Intent(this, dest);

        i.putParcelableArrayListExtra("data", data);

        startActivity(i);

        AgregarContacto.this.finish();
    }


    private boolean validarControles() {
        boolean continuar = true;
        String mensaje = "";


        if(fecha.getText().toString().isEmpty()){
            continuar = false;
            mensaje = "Debe ingresar una fecha.";
        }

        if(direccion.getText().toString().isEmpty()){
            continuar = false;
            mensaje = "Debe ingresar una dirección.";
        }

        if(mail.getText().toString().isEmpty()){
            continuar = false;
            mensaje = "Debe ingresar un email.";
        }
        if (telefono.getText().toString().isEmpty()) {
            continuar = false;
            mensaje = "Debe ingresar un teléfono.";
        }

        if (apellido.getText().toString().isEmpty()) {
            continuar = false;
            mensaje = "Debe ingresar un apellido.";
        }

        if (nombre.getText().toString().isEmpty()) {
            continuar = false;
            mensaje = "Debe ingresar un nombre.";
        }



        if(!continuar){
            Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
            return false;
        }

        String correo = mail.getText().toString();

        if(!Patterns.EMAIL_ADDRESS.matcher(correo).matches()){
            Toast.makeText(this, "El correo ingresado no es valido.", Toast.LENGTH_SHORT).show();
            return false;
        }

        String date = fecha.getText().toString();

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        try {
            format.parse(date);
        }
        catch(ParseException e){
            Toast.makeText(this, "La fecha ingresada no es valida.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }


}