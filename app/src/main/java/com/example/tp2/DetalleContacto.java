package com.example.tp2;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DetalleContacto  extends AppCompatActivity {

    String dataFile = "data.obj";
    private TextView txtNombre,txtApellido,txtTelefono,txtEmail,txtDireccion,txtFecha,txtNivel,txtInteres,txtInfo;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_contacto);

        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtApellido = (TextView) findViewById(R.id.txtApellido);
        txtTelefono = (TextView) findViewById(R.id.txtTelefono);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtDireccion = (TextView) findViewById(R.id.txtDireccion);
        txtFecha = (TextView) findViewById(R.id.txtFecha);
        txtNivel = (TextView) findViewById(R.id.txtNivel);
        txtInteres = (TextView) findViewById(R.id.txtInteres);
        txtInfo = (TextView) findViewById(R.id.txtInfo);

        int p = getIntent().getIntExtra("posicion",-1);
        //String msg = "la posicion es " + Integer.toString(p);
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

        try {
            ObjectInputStream objInput = new ObjectInputStream(openFileInput(dataFile));
            Persona[] persona = (Persona[]) objInput.readObject();
            objInput.close();

            txtNombre.setText(persona[p].getNombre());
            txtApellido.setText(persona[p].getApellido());
            txtTelefono.setText(persona[p].getTelefono() +" - "+ persona[p].getTelefonoLugar());
            txtEmail.setText(persona[p].getEmail() + " - " + persona[p].getEmailLugar());
            txtDireccion.setText(persona[p].getDireccion());
            txtFecha.setText(persona[p].getFechaNac());
            txtNivel.setText(persona[p].getEstudios());
            txtInteres.setText(persona[p].getIntereses());
            txtInfo.setText(persona[p].isEnviarInfo());


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

    }
}
