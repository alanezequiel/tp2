package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Ejercicio Controles - Grupo X");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_activity, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem option){

        int id = option.getItemId();

        if(id == R.id.agregar_contacto){
            this.exec_activity(null, AgregarContacto.class);
            return true;
        }
        if(id == R.id.listar_contactos){
            this.exec_activity(null, ListarContactos.class);
            return true;
        }

        return super.onOptionsItemSelected(option);
    }

    private void exec_activity(View view, Class dest){
        Intent i = new Intent(this, dest);
        startActivity(i);
    }
}