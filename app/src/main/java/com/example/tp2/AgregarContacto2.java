package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class AgregarContacto2 extends AppCompatActivity {

    ArrayList<FormData> formData;
    String dataFile = "data.obj";

    private RadioGroup radioGroup;
    private Button btnGuardar;
    private Switch aSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agregar_contacto2);
        setTitle("EjercicioControles");

        radioGroup = findViewById(R.id.rgEstudios);
        aSwitch = findViewById(R.id.switch2);
        btnGuardar = findViewById(R.id.btnGuardar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validarControles()) {
                    ArrayList<FormData> data = new ArrayList<FormData>(formData);
                    for (int i = 0; i < formData.size(); i++) {
                        if (formData.get(i).getValue().length() == 0) {
                            data.get(i).setValue(getInputText(data.get(i)));
                        }
                    }
                    writeDataToFile();
                }
            }
        });

        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(aSwitch.isChecked()){
                    aSwitch.setText("Si");
                }else{
                    aSwitch.setText("No");
                }
            }
        });

        formData = new ArrayList<FormData>();
        Intent intent = getIntent();
        formData = intent.getParcelableArrayListExtra("data");

        formData.add(new FormData("RadioGroup","rgEstudios","", "Estudios", 8, 6));
        formData.add(new FormData("CheckBox", "checkBox", "", "", 9,0));
        formData.add(new FormData("CheckBox", "checkBox2", "", "", 9,0));
        formData.add(new FormData("CheckBox", "checkBox3", "", "", 9,0));
        formData.add(new FormData("CheckBox", "checkBox4", "", "", 9,0));
        formData.add(new FormData("Switch", "switch2", "", "", 10,0));
    }

    private String getInputText(FormData node){
        String resp = "";
        int resourceId = getResources().getIdentifier(node.getName(),"id", this.getPackageName());
        switch (node.getType()) {
            case "Switch":
                Switch swt = (Switch) findViewById(resourceId);
                resp = swt.getText().toString();
                break;
            case "RadioGroup":
                RadioGroup radioGroup = (RadioGroup) findViewById(resourceId);
                int id = radioGroup.getCheckedRadioButtonId();
                if (id != -1) {
                    RadioButton radioButton = (RadioButton) findViewById(id);
                    resp = radioButton.getText().toString();
                }
                break;
            case "CheckBox":
                CheckBox checkBox = (CheckBox) findViewById(resourceId);
                if(checkBox.isChecked()){
                    resp = checkBox.getText().toString();
                }
        }
        return resp;
    }

    private void writeDataToFile() {
        Persona persona = new Persona();
        persona = createPersona();
        try{

            if(ArchivoExiste(dataFile)){
                Persona[] contactos = CargarArray(persona);
                ObjectOutputStream objOutput = new ObjectOutputStream(openFileOutput(this.dataFile, MODE_PRIVATE));
                objOutput.writeObject(contactos);
                objOutput.close();
            }else{
                Persona[] contactos = {persona};
                ObjectOutputStream objOutput = new ObjectOutputStream(openFileOutput(this.dataFile, MODE_PRIVATE));
                objOutput.writeObject(contactos);
                objOutput.close();
            }

        }catch (IOException e){
            e.printStackTrace();
            String msj ="Error al guardar el archivo: " + e.getMessage();
            Toast.makeText(getApplicationContext(), msj, msj.length()).show();
        }
        Toast.makeText(this, "Contacto Guardado Correctamente", Toast.LENGTH_SHORT).show();
        AgregarContacto2.this.finish();
    }

    private Persona createPersona(){
        Persona p = new Persona();
        for(int i = 0; i < this.formData.size(); i++){
            String value = formData.get(i).getValue();
            if(value.length() > 0){
                p.set( formData.get(i).getIndex(), value );
            }
        }
        return p;
    }



    public Persona[] CargarArray(Persona NuevoObj){
        Persona[] contactos,a = new Persona[0];
        try{
            ObjectInputStream objInput = new ObjectInputStream(openFileInput(dataFile));
            Persona persona[] = (Persona[]) objInput.readObject();
            contactos = new Persona[persona.length + 1];
            objInput.close();
            int h = persona.length;
            for(int i = 0; i < h; i++){
                contactos[i] = persona[i];
            }
            contactos[h] = NuevoObj;
            return contactos;
        }catch (IOException e){
            Toast.makeText(this, "Error al cargar archivo", Toast.LENGTH_SHORT).show();
        }catch (ClassNotFoundException e){
            Log.e("MainActivity", "Error clase no encontrada");
        }
        return a;
    }

    public boolean ArchivoExiste(String NombreArchivo){
        String[] archivos = fileList();
        for (int i=0; i < archivos.length; i++){
            if(NombreArchivo.equals(archivos[i])){
                return true;
            }
        }
        return false;
    }

    private boolean validarControles(){
        if(radioGroup.getCheckedRadioButtonId() == -1){
            Toast.makeText(this, "Debe seleccionar un nivel de estudio.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}